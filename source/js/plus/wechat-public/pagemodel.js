var Backbone=require('backbone');
var modelExtend={
    defaults:{
        pageNow:1,
        pageStart:1,
        pageEnd:1,
        startX:0,
        startY:0,
        endX:0,
        endY:0,
        direction:'down'
    }
};
module.exports=Backbone.Model.extend(modelExtend);