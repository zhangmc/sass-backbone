var PageModel=require('./pagemodel');
var Backbone=require('backbone');
var PageView=require('./pageview');
var pageModel=new PageModel();
var AppRouter=Backbone.Router.extend({
    routes:{
        'p/:num':'switchPage',
        '*actions':'toIndex'
    },
    toIndex:function(){
        this.navigate('p/1');
    },
    switchPage:function(num){
        num=parseInt(num,10);
        var pageStart=pageModel.get('pageStart');
        var pageEnd=pageModel.get('pageEnd');
        if(isNaN(num)||num<pageStart||num>pageEnd){
            return this.toIndex();
        }
        pageModel.set('pageNow',num);
        if(num===1){
            pageModel.trigger('change:pageNow');
        }
    }
});
var appRouter=new AppRouter();
new PageView({model:pageModel,appRouter:appRouter});
Backbone.history.start();