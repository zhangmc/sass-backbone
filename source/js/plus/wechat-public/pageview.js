var Backbone=require('backbone');
var aImage2Load=require('./image2load');
var viewExtend={
    initialize:function(opt){
        var that=this;
        that.appRouter=opt.appRouter;
        that.setElement('#j-app');
        that.imgPathPrefix=pageInfo.STATIC_PATH+'images/wap/wechat-public/publicize/';
        that.domSet={
            oPercentage:that.fnGetDOM('#j-progress-percentage'),
            oPercentageNum:that.fnGetDOM('#j-percentage-num'),
            oAniContainer:that.fnGetDOM('#j-ani-container'),
            pages:{
                landscapeTips:that.fnGetDOM('#j-landscape-tips')
            }
        };
        var pageArr=that.el.querySelectorAll('.j-page');
        that.model.set('pageEnd',pageArr.length);
        for(var i=0,l=pageArr.length;i<l;i++){
            that.domSet.pages[i+1]=pageArr[i];
        }
        that.listenTo(that.model,'change:pageNow',that.fnSwitchPage);
        that.aImageLoaded=[];
        that.fnLoadImage();
    },
    fnGetDOM:function(selector){
        return document.querySelector(selector);
    },
    events:{
        'touchstart #j-ani-container':'touchStartHandle',
        'touchend #j-ani-container':'touchEndHandle'
    },
    fnCalculateLoadPercentage:function(){
        var that=this;
        var nPercentage=that.aImageLoaded.length/aImage2Load.length*100;
        var sPercentage=nPercentage.toFixed(0)+'%';
        that.domSet.oPercentage.style.width=sPercentage;
        that.domSet.oPercentageNum.innerHTML=sPercentage;
    },
    fnLoadImage:function(){
        var image=new Image();
        var that=this;
        var aImageLoaded=that.aImageLoaded;
        image.onload=function(){
            aImageLoaded[aImageLoaded.length]=image.src;
            that.fnCalculateLoadPercentage();
            if(aImage2Load.length>aImageLoaded.length){
                return that.fnLoadImage();
            }else{
                setTimeout(function(){
                    that.fnGetDOM('#j-loading').classList.add('none');
                    that.domSet.oAniContainer.classList.remove('none');
                },800);
            }
        };
        image.src=that.imgPathPrefix+aImage2Load[aImageLoaded.length];
    },
    fnSwitchPage:function(){
        this.clearPreviousAni();
        this.fnSwitch2CurrentPage();
    },
    fnSwitch2CurrentPage:function(){
        var that=this;
        var nextPage=that.model.get('pageNow');
        var direction=that.model.get('direction');
        var oNextPage=that.domSet.pages[nextPage];
        oNextPage.classList.add('animate');
        oNextPage.classList.add(direction);
        oNextPage.classList.add('play');
        oNextPage.classList.remove('none');
    },
    clearPreviousAni:function(){
        var that=this;
        var previousPage=that.model.previous('pageNow');
        var nextPage=that.model.get('pageNow');
        var oPreviousPage=that.domSet.pages[previousPage];
        oPreviousPage.classList.add('none');
        oPreviousPage.classList.remove('animate');
        oPreviousPage.classList.remove('down');
        oPreviousPage.classList.remove('up');
        oPreviousPage.classList.remove('play');
    },
    fnCalculateSwipDirection:function(){
        var that=this;
        var modelJSON=that.model.toJSON();
        var nDistance=modelJSON.endY-modelJSON.startY;
        if(nDistance>0){
            modelJSON.direction='down';
            --modelJSON.pageNow;
        }else if(nDistance<0){
            modelJSON.direction='up';
            ++modelJSON.pageNow;
        }
        modelJSON.pageNow=modelJSON.pageNow>modelJSON.pageEnd?modelJSON.pageStart:modelJSON.pageNow;
        modelJSON.pageNow=modelJSON.pageNow<modelJSON.pageStart?modelJSON.pageEnd:modelJSON.pageNow;
        that.model.set(modelJSON);
        that.appRouter.navigate('p/'+modelJSON.pageNow);
    },
    touchStartHandle:function(e){
        var that=this;
        var coord={
            startX:e.changedTouches[0].clientX,
            startY:e.changedTouches[0].clientY
        };
        that.model.set(coord);
    },
    touchEndHandle:function(e){
        var that=this;
        var coord={
            endX:e.changedTouches[0].clientX,
            endY:e.changedTouches[0].clientY
        };
        that.model.set(coord);
        that.fnCalculateSwipDirection();
    }
};
module.exports=Backbone.View.extend(viewExtend);