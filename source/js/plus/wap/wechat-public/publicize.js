/**
 * Created by seiya on 15/6/3.
 */
(function(){
    var fnPreventBodyScroll=function(e){
        e.preventDefault();
    };
    var fnDisabledBodyScroll=function(){
        document.addEventListener('touchmove',fnPreventBodyScroll,false);
    };

    var imgPathPrefix=pageInfo.STATIC_PATH+'images/wap/wechat-public/publicize/';
    var aImage2Load=[
        'p4/bg.png',
        'p4/figure.png',
        'p4/qrcode.png',
        'p4/text/laidoulaile.png',
        'p4/text/mashangguanzhu.png',
        'p4/text/quanzhangwo.png',
        'p4/text/shibieerweima.png',
        'p4/text/tianyiyonghuzhongxin.png',
        //p3
        'p3/bg.png',
        'p3/human.png',
        'p3/text/buyongpaidui.png',
        'p3/text/da.png',
        'p3/text/guanzhugongzhonghao.png',
        'p3/text/jia.png',
        'p3/text/tanhao.png',
        'p3/text/xian.png',
        'p3/text/zhangdanchaxun.png',
        'p3/text/zhegegongne.png',
        'p3/text/zhidao.png',
        //p2
        'p2/bg.png',
        'p2/human.png',
        'p2/text/dao.png',
        'p2/text/guanzhugongzhonghao.png',
        'p2/text/gui.png',
        'p2/text/haiyouhuafei.png',
        'p2/text/huafeiyue.png',
        'p2/text/mi.png',
        'p2/text/tanhao.png',
        'p2/text/xian.png',
        'p2/text/zhegegongne.png',
        'p2/text/zhi.png',
        //p1
        'p1/bg.png',
        'p1/downarrow.png',
        'p1/human-l.png',
        'p1/human-s.png',
        'p1/soap.png',
        'p1/star-l.png',
        'p1/star-s.png',
        'p1/water.png',
        'p1/text/dao.png',
        'p1/text/guanzhugongzhonghao.png',
        'p1/text/ji.png',
        'p1/text/liuliangchaobiao.png',
        'p1/text/liuliangshiyong.png',
        'p1/text/tanhao.png',
        'p1/text/xian.png',
        'p1/text/you.png',
        'p1/text/zhegegongne.png',
        'p1/text/zhi.png'
    ];
    var aImageLoaded=[];
    var fnGetDOM=function(selector){
        return document.querySelector(selector);
    };
    var oPercentage=fnGetDOM('#j-progress-percentage');
    var oPercentageNum=fnGetDOM('#j-percentage-num');
    var fnCalculateLoadPercentage=function(){
        var nPercentage=aImageLoaded.length/aImage2Load.length*100;
        var sPercentage=nPercentage.toFixed(0)+'%';
        oPercentage.style.width=sPercentage;
        oPercentageNum.innerHTML=sPercentage;
    };
    var oAniContainer=fnGetDOM('#j-ani-container');
    var oTouches={
        startX:0,
        startY:0,
        endX:0,
        endY:0
    };
    var oPage={
        pageNow:1,
        pageStart:1,
        pageEnd:4
    };
    var oDomCache={
        1:fnGetDOM('#j-page-1'),
        2:fnGetDOM('#j-page-2'),
        3:fnGetDOM('#j-page-3'),
        4:fnGetDOM('#j-page-4'),
        landscapeTips:fnGetDOM('#j-landscape-tips')
    };
    var clearPreviousAni=function(previousPage){
        var oPreviousPage=oDomCache[previousPage];
        oPreviousPage.classList.add('none');
        oPreviousPage.classList.remove('animate');
        oPreviousPage.classList.remove('down');
        oPreviousPage.classList.remove('up');
        oPreviousPage.classList.remove('play');
    };
    var fnSwitchPage=function(nextPage,direction){
        var oNextPage=oDomCache[nextPage];
        oNextPage.classList.add('animate');
        oNextPage.classList.add(direction||'up');
        oNextPage.classList.add('play');
        oNextPage.classList.remove('none');
    };
    var fnCalculateSwipDirection=function(oTouches){
        var nDistance=oTouches.endY-oTouches.startY;
        if(nDistance>0){
            //向下
            --oPage.pageNow;
            if(oPage.pageNow>=oPage.pageStart){
                clearPreviousAni(oPage.pageNow+1);
                fnSwitchPage(oPage.pageNow,'down');

            }else{
                oPage.pageNow=oPage.pageStart;
            }

        }else if(nDistance<0){
            //向上
            ++oPage.pageNow;
            if(oPage.pageNow<=oPage.pageEnd){
                clearPreviousAni(oPage.pageNow-1);
                fnSwitchPage(oPage.pageNow,'up');

            }else{
                oPage.pageNow=oPage.pageEnd;
            }
        }
    };
    var fnLoadImage=function(){
        var image=new Image();
        image.onload=function(){
            aImageLoaded[aImageLoaded.length]=image.src;
            fnCalculateLoadPercentage();
            if(aImage2Load.length>aImageLoaded.length){
                return fnLoadImage();
            }else{
                setTimeout(function(){
                    fnGetDOM('#j-loading').classList.add('none');
                    oAniContainer.classList.remove('none');
                    fnSwitchPage(oPage.pageStart);
                },800);
            }
        };
        image.src=imgPathPrefix+aImage2Load[aImageLoaded.length];
    };
    fnLoadImage();
    if(/micromessenger/i.test(navigator.userAgent)){
        fnDisabledBodyScroll();
    }
    oAniContainer.addEventListener('touchstart',function(e){
        oTouches.startX=e.changedTouches[0].clientX;
        oTouches.startY=e.changedTouches[0].clientY;
    },false);

    oAniContainer.addEventListener('touchend',function(e){
        oTouches.endX=e.changedTouches[0].clientX;
        oTouches.endY=e.changedTouches[0].clientY;
        fnCalculateSwipDirection(oTouches);
    },false);
    //横屏提示
    var fnShowLandscapeTips=function(){
        if(typeof window.orientation!=='undefined'){
            var bShowTips=window.orientation===0;
            oAniContainer.style.display=!bShowTips?'none':'block';
            oDomCache.landscapeTips.style.display=bShowTips?'none':'block';
        }

    };
    fnShowLandscapeTips();
    document.addEventListener('orientationchange',fnShowLandscapeTips,false);
})();
