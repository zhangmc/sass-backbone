define("plus/wechat-public/0.0.1/index-debug", ["backbone"], function(require, exports, module){
var PageModel=require("plus/wechat-public/0.0.1/pagemodel-debug");
var Backbone=require("backbone");
var PageView=require("plus/wechat-public/0.0.1/pageview-debug");
var pageModel=new PageModel();
var AppRouter=Backbone.Router.extend({
    routes:{
        'p/:num':'switchPage',
        '*actions':'toIndex'
    },
    toIndex:function(){
        this.navigate('p/1');
    },
    switchPage:function(num){
        num=parseInt(num,10);
        var pageStart=pageModel.get('pageStart');
        var pageEnd=pageModel.get('pageEnd');
        if(isNaN(num)||num<pageStart||num>pageEnd){
            return this.toIndex();
        }
        pageModel.set('pageNow',num);
        if(num===1){
            pageModel.trigger('change:pageNow');
        }
    }
});
var appRouter=new AppRouter();
new PageView({model:pageModel,appRouter:appRouter});
Backbone.history.start();
});
define("plus/wechat-public/0.0.1/pagemodel-debug", ["backbone"], function(require, exports, module){
var Backbone=require("backbone");
var modelExtend={
    defaults:{
        pageNow:1,
        pageStart:1,
        pageEnd:1,
        startX:0,
        startY:0,
        endX:0,
        endY:0,
        direction:'down'
    }
};
module.exports=Backbone.Model.extend(modelExtend);
});
define("plus/wechat-public/0.0.1/pageview-debug", ["backbone"], function(require, exports, module){
var Backbone=require("backbone");
var aImage2Load=require("plus/wechat-public/0.0.1/image2load-debug");
var viewExtend={
    initialize:function(opt){
        var that=this;
        that.appRouter=opt.appRouter;
        that.setElement('#j-app');
        that.imgPathPrefix=pageInfo.STATIC_PATH+'images/wap/wechat-public/publicize/';
        that.domSet={
            oPercentage:that.fnGetDOM('#j-progress-percentage'),
            oPercentageNum:that.fnGetDOM('#j-percentage-num'),
            oAniContainer:that.fnGetDOM('#j-ani-container'),
            pages:{
                landscapeTips:that.fnGetDOM('#j-landscape-tips')
            }
        };
        var pageArr=that.el.querySelectorAll('.j-page');
        that.model.set('pageEnd',pageArr.length);
        for(var i=0,l=pageArr.length;i<l;i++){
            that.domSet.pages[i+1]=pageArr[i];
        }
        that.listenTo(that.model,'change:pageNow',that.fnSwitchPage);
        that.aImageLoaded=[];
        that.fnLoadImage();
    },
    fnGetDOM:function(selector){
        return document.querySelector(selector);
    },
    events:{
        'touchstart #j-ani-container':'touchStartHandle',
        'touchend #j-ani-container':'touchEndHandle'
    },
    fnCalculateLoadPercentage:function(){
        var that=this;
        var nPercentage=that.aImageLoaded.length/aImage2Load.length*100;
        var sPercentage=nPercentage.toFixed(0)+'%';
        that.domSet.oPercentage.style.width=sPercentage;
        that.domSet.oPercentageNum.innerHTML=sPercentage;
    },
    fnLoadImage:function(){
        var image=new Image();
        var that=this;
        var aImageLoaded=that.aImageLoaded;
        image.onload=function(){
            aImageLoaded[aImageLoaded.length]=image.src;
            that.fnCalculateLoadPercentage();
            if(aImage2Load.length>aImageLoaded.length){
                return that.fnLoadImage();
            }else{
                setTimeout(function(){
                    that.fnGetDOM('#j-loading').classList.add('none');
                    that.domSet.oAniContainer.classList.remove('none');
                },800);
            }
        };
        image.src=that.imgPathPrefix+aImage2Load[aImageLoaded.length];
    },
    fnSwitchPage:function(){
        this.clearPreviousAni();
        this.fnSwitch2CurrentPage();
    },
    fnSwitch2CurrentPage:function(){
        var that=this;
        var nextPage=that.model.get('pageNow');
        var direction=that.model.get('direction');
        var oNextPage=that.domSet.pages[nextPage];
        oNextPage.classList.add('animate');
        oNextPage.classList.add(direction);
        oNextPage.classList.add('play');
        oNextPage.classList.remove('none');
    },
    clearPreviousAni:function(){
        var that=this;
        var previousPage=that.model.previous('pageNow');
        var nextPage=that.model.get('pageNow');
        var oPreviousPage=that.domSet.pages[previousPage];
        oPreviousPage.classList.add('none');
        oPreviousPage.classList.remove('animate');
        oPreviousPage.classList.remove('down');
        oPreviousPage.classList.remove('up');
        oPreviousPage.classList.remove('play');
    },
    fnCalculateSwipDirection:function(){
        var that=this;
        var modelJSON=that.model.toJSON();
        var nDistance=modelJSON.endY-modelJSON.startY;
        if(nDistance>0){
            modelJSON.direction='down';
            --modelJSON.pageNow;
        }else if(nDistance<0){
            modelJSON.direction='up';
            ++modelJSON.pageNow;
        }
        modelJSON.pageNow=modelJSON.pageNow>modelJSON.pageEnd?modelJSON.pageStart:modelJSON.pageNow;
        modelJSON.pageNow=modelJSON.pageNow<modelJSON.pageStart?modelJSON.pageEnd:modelJSON.pageNow;
        that.model.set(modelJSON);
        that.appRouter.navigate('p/'+modelJSON.pageNow);
    },
    touchStartHandle:function(e){
        var that=this;
        var coord={
            startX:e.changedTouches[0].clientX,
            startY:e.changedTouches[0].clientY
        };
        that.model.set(coord);
    },
    touchEndHandle:function(e){
        var that=this;
        var coord={
            endX:e.changedTouches[0].clientX,
            endY:e.changedTouches[0].clientY
        };
        that.model.set(coord);
        that.fnCalculateSwipDirection();
    }
};
module.exports=Backbone.View.extend(viewExtend);
});
define("plus/wechat-public/0.0.1/image2load-debug", [], function(require, exports, module){
module.exports=[
    'p4/bg.png',
    'p4/figure.png',
    'p4/qrcode.png',
    'p4/text/laidoulaile.png',
    'p4/text/mashangguanzhu.png',
    'p4/text/quanzhangwo.png',
    'p4/text/shibieerweima.png',
    'p4/text/tianyiyonghuzhongxin.png',
    //p3
    'p3/bg.png',
    'p3/human.png',
    'p3/text/buyongpaidui.png',
    'p3/text/da.png',
    'p3/text/guanzhugongzhonghao.png',
    'p3/text/jia.png',
    'p3/text/tanhao.png',
    'p3/text/xian.png',
    'p3/text/zhangdanchaxun.png',
    'p3/text/zhegegongne.png',
    'p3/text/zhidao.png',
    //p2
    'p2/bg.png',
    'p2/human.png',
    'p2/text/dao.png',
    'p2/text/guanzhugongzhonghao.png',
    'p2/text/gui.png',
    'p2/text/haiyouhuafei.png',
    'p2/text/huafeiyue.png',
    'p2/text/mi.png',
    'p2/text/tanhao.png',
    'p2/text/xian.png',
    'p2/text/zhegegongne.png',
    'p2/text/zhi.png',
    //p1
    'p1/bg.png',
    'p1/downarrow.png',
    'p1/human-l.png',
    'p1/human-s.png',
    'p1/soap.png',
    'p1/star-l.png',
    'p1/star-s.png',
    'p1/water.png',
    'p1/text/dao.png',
    'p1/text/guanzhugongzhonghao.png',
    'p1/text/ji.png',
    'p1/text/liuliangchaobiao.png',
    'p1/text/liuliangshiyong.png',
    'p1/text/tanhao.png',
    'p1/text/xian.png',
    'p1/text/you.png',
    'p1/text/zhegegongne.png',
    'p1/text/zhi.png'
];

});
